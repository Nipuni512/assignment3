#include <stdio.h>
int main()
{
  char c;
  printf ("Enter a Letter: ");
  scanf ("%c", &c);
  if (c=='A'||c=='E'||c=='I'||c=='O'||c=='U'||c=='a'||c=='e'||c=='i'||c=='o'||c=='u')
      printf ("\n%c is a Vowel", c);
  else
      printf ("\n%c is a Consonant", c);
  return 0;
}
